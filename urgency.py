# -*- coding: utf-8 -*-
"""
Created on Fri Apr 20 15:51:31 2018

@author: enovi
"""

#Urgency
import nltk

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Urgency Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             analyze_text(article_text)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def analyze_text(article_text):
    
    urg_list = ['abrupt', 'acute', 'alacrity', 'alert', 'anticipated', 'apprise', 'approach',
                'asap', 'barrel', 'bolt', 'boogie', 'bound', 'brewing', 'clip', 'close', 'compel',
                'contemporary', 'crucial', 'current', 'dash', 'desperate', 'dire', 'drastic',
                'essential', 'exhort', 'exigent', 'expected', 'expedite', 'extreme', 'fickle',
                'flash', 'fleet', 'float', 'fluctuate', 'forthcoming', 'fresh', 'gallop', 'grave',
                'haste', 'hurry', 'hurtle', 'immediate', 'imminent', 'impacting', 'impend',
                'imperative', 'important', 'inform', 'instant', 'intense', 'late', 'latter',
                'lightning', 'looming', 'mandatory', 'menacing', 'modern', 'move', 'mutable',
                'near', 'necessary', 'neoteric', 'new', 'nigh', 'notify', 'now', 'overnight',
                'parlous', 'pressing', 'priority', 'prompt', 'quick', 'rapid', 'rathe', 'remind',
                'run', 'rush', 'scamper', 'scoot', 'scramble', 'scurry', 'scuttle', 'serious',
                'shake', 'shift', 'snap', 'soon', 'speed', 'sudden', 'tell', 'test', 'threatening',
                'tip', 'uncertain', 'upcoming', 'urge', 'vacillate', 'vary', 'vital', 'volatile',
                'warn', 'whip', 'whirlwind', 'zip', 'zoom']
    
    urg_score = 0
    urg_count = 0
    
    article_text = article_text.lower().split()
    
    #stemming
    for word in article_text:
        word = nltk.PorterStemmer().stem(word)
        
    for word in urg_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in article_text:
        if word in urg_list:
            urg_count += 1

    if len(article_text) > 0 and urg_count > 0:
        urg_score = urg_count / len(article_text)
        if urg_score > 0.01:
            print('The message was very urgent.')
        if urg_score <= 0.01 and urg_score >= 0.005:
            print('The message was moderately urgent.')
        if urg_score < 0.005:
            print('The message was not urgent.')
            
    else:
        print('No urgency keywords were detected.')    
    
main()
    